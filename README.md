# simple_interest_app

Simple Interest Application
- basic application to showcase custom reusable widget, and Forms to validate input

![Screenshot_1594218964](/uploads/8a3ff46ddc9df18fb7e49b8d88f3512f/Screenshot_1594218964.png)

![Screenshot_1594218929](/uploads/b47c6d7a8f5be66fac44ce527559f52f/Screenshot_1594218929.png)

![Screenshot_1594218953](/uploads/aa8d97f15ac19b56d4cc027cea7a0c7d/Screenshot_1594218953.png)



## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
