import 'package:flutter/material.dart';

class EditText extends StatefulWidget {
  final ValueChanged<String> onChanged;
  final String label;
  final String hint;
  final EdgeInsetsGeometry padding;
  final TextStyle textStyle;
  final TextInputType keyboardType;
  final TextEditingController textEditingController;

  const EditText({
    this.onChanged,
    this.label,
    this.hint,
    this.padding,
    this.textStyle,
    this.keyboardType,
    this.textEditingController,
  }) /*
      : assert(label != null,
  hint != null)*/
  ;

  @override
  State<StatefulWidget> createState() {
    final decoration = InputDecoration(
        labelText: label,
        hintText: hint,
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
        errorStyle: TextStyle(
          fontSize: 10.0,
          color: Colors.yellow,
        ));
    return EditTextState(decoration);
  }
}

class EditTextState extends State<EditText> {
  final InputDecoration decoration;
  EditTextState(this.decoration);

  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = Theme.of(context).textTheme.title;
    return Container(
      padding: widget.padding ?? EdgeInsets.all(10.0),
      child: TextFormField(
        style: widget.textStyle ?? textStyle,
        keyboardType: widget.keyboardType,
        decoration: decoration,
        controller: widget.textEditingController,
        validator: (String userInput) {
          if (userInput.isEmpty) {
            return 'Please enter ${decoration.labelText}';
          }
          return null;
        },
        onChanged: (String userInPut) {
          setState(() {
            widget.onChanged(userInPut);
          });
        },
      ),
    );
  }
}
