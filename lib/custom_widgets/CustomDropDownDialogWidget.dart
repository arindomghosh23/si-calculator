import 'package:flutter/material.dart';
import 'package:simple_interest_app/custom_widgets/CustomText.dart';

class CustomDropDownDialogWidget extends StatefulWidget {
  final ValueChanged<String> onChanged;
  final List<String> items;

  CustomDropDownDialogWidget({this.items, this.onChanged})
      : assert(items != null|| items.isNotEmpty);

  @override
  State<StatefulWidget> createState() {
    return CurrencyState(items);
  }
}

class CurrencyState extends State<CustomDropDownDialogWidget> {
  var _userSelection = "";
  var items = List<String>();

  CurrencyState(this.items){
    _userSelection = items[0];
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: DropdownButton<String>(
        items:items.map((String mDialogueItem) {
          return DropdownMenuItem<String>(
            value: mDialogueItem,
            child: CustomTextView(mDialogueItem),
          );
        }).toList(),
        onChanged: (String userSelection) {
          setState(() {
            widget.onChanged(userSelection);
            _userSelection = userSelection;
          });
        },
        value: _userSelection,
      ),
    );
  }
}
