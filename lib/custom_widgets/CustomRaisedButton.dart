import 'package:flutter/material.dart';

class CustomRaisedButton extends StatefulWidget {
  final VoidCallback onPressed;
  final String name;
  final Color color;
  final Color textColor;

  const CustomRaisedButton(
      {@required this.name, this.onPressed, this.color, this.textColor})
      : assert(name != null);

  @override
  State<StatefulWidget> createState() {
    return CustomRaisedButtonState();
  }
}

class CustomRaisedButtonState extends State<CustomRaisedButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: RaisedButton(
        color: widget.color,
        textColor: widget.textColor,
        child: Text(
          widget.name,
          textScaleFactor: 1.5,
        ),
        onPressed: () {
          widget.onPressed();
        },
      ),
    );
  }
}
