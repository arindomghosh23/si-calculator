import 'package:flutter/material.dart';

class ImageView extends StatelessWidget {
  final String imagePath;

  const ImageView(this.imagePath) :assert(imagePath != null);

  @override
  Widget build(BuildContext context) {
    var imageAsset = AssetImage(imagePath);
    var mImage = Image(image: imageAsset,);
    return Container(
      padding: EdgeInsets.all(10.0),
      child: mImage,
    );
  }
}