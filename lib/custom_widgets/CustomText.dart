import 'package:flutter/material.dart';

class CustomTextView extends StatelessWidget {
  final String name;
  final TextStyle textStyle;

  const CustomTextView(this.name,{this.textStyle})
      : assert(name != null);

  @override
  Widget build(BuildContext context) {
    final TextStyle _textStyle = Theme.of(context).textTheme.title;
    return Text(
      name,
      style: textStyle ?? _textStyle,
      textAlign: TextAlign.center,
    );
  }
}
