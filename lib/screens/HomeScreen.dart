import 'package:flutter/material.dart';
import 'package:simple_interest_app/custom_widgets/CustomDropDownDialogWidget.dart';
import 'package:simple_interest_app/custom_widgets/CustomRaisedButton.dart';
import 'package:simple_interest_app/custom_widgets/CustomText.dart';
import 'package:simple_interest_app/custom_widgets/EditText.dart';
import 'package:simple_interest_app/custom_widgets/ImageView.dart';
import 'package:simple_interest_app/repositories/GetData.dart';

class MainScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MainScreenState();
  }
}

class MainScreenState extends State<MainScreen> {
  var _currencies = GetData().getCurrencyList();
  var _formKey = GlobalKey<FormState>();
  final TextEditingController principalController = TextEditingController();
  final TextEditingController roiController = TextEditingController();
  final TextEditingController termController = TextEditingController();

  var _currentItemSelected = '';
  var _result = '';

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Padding(
        padding: EdgeInsets.all(20.0),
        child: ListView(
          children: <Widget>[
            Center(
              child: ImageView('images/logo_qzone.png'),
            ),
            Center(
              child: EditText(
                label: 'Principal',
                hint: 'Enter Principle e.g. Rs.20000',
                keyboardType: TextInputType.numberWithOptions(),
                textEditingController: principalController,
              ),
            ),
            Center(
              child: EditText(
                label: 'Rate of Interest',
                hint: 'In Percent',
                keyboardType: TextInputType.numberWithOptions(),
                textEditingController: roiController,
              ),
            ),
            Center(
              child: Row(
                children: <Widget>[
                  Expanded(
                      child: EditText(
                        label: 'Term ',
                        hint: 'In Years',
                        keyboardType: TextInputType.numberWithOptions(),
                        textEditingController: termController,
                      )),
                  Expanded(
                    child: CustomDropDownDialogWidget(
                      items: _currencies,
                      onChanged: (String selectedCurrency) {
                        setState(() {
                          _currentItemSelected = selectedCurrency;
//                        debugPrint(selectedCurrency);
                        });
                      },
                    ),
                  )
                ],
              ),
            ),
            Center(
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: CustomRaisedButton(
                      name: 'Calculate',
                      color: Theme
                          .of(context)
                          .accentColor,
                      textColor: Theme
                          .of(context)
                          .primaryColorDark,
                      onPressed: () {
                        setState(() {
                          if (_formKey.currentState.validate()) {
                            _result = _calculateTotalReturn();
                          }
                        });
                      },
                    ),
                  ),
                  Expanded(
                    child: CustomRaisedButton(
                      name: 'Reset',
                      color: Theme
                          .of(context)
                          .primaryColorDark,
                      textColor: Theme
                          .of(context)
                          .primaryColorLight,
                      onPressed: () {
                        setState(() {
                          _reset();
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
            Center(
              child: Container(
                margin: EdgeInsets.all(10.0),
                child: CustomTextView(this._result),
              ),
            )
          ],
        ),
      ),

    );
  }

  String _calculateTotalReturn() {
    var principle = double.parse(principalController.text);
    var roi = double.parse(roiController.text);
    var term = double.parse(termController.text);
    var totalAmountPayable = (principle * roi * term) / 100;

    return 'After $term years, your investment will be worth $totalAmountPayable $_currentItemSelected';
  }

  void _reset() {
    principalController.text = '';
    roiController.text = '';
    termController.text = '';
    _result = '';
    _currentItemSelected = _currencies[0];
  }
}
