class GetData {
  static final GetData _cache = GetData._internal();

  factory GetData() {
    return _cache;
  }

  GetData._internal();

  List<String> getCurrencyList() {
    return ['USD', 'INR', 'AED', 'SGD', 'UKP', 'Others'];
  }
}
